package com.test.uklon

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.test.uklon.api.models.Post
import com.test.uklon.util.inflate
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter(val posts: List<Post>, val listener: (Post) -> Unit) : RecyclerView.Adapter<PostAdapter.PostHolder>() {

    override fun getItemCount(): Int = posts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder = PostHolder(parent.inflate(R.layout.item_post))

    override fun onBindViewHolder(holder: PostHolder, position: Int) = holder.bind(posts[position], listener)

    data class PostHolder(val view: View) : RecyclerView.ViewHolder(view){

        fun bind(post: Post , listener: (Post) -> Unit) = with(view){
            view.title.text = post.title
            view.body.text = post.body
            setOnClickListener { listener(post) }
        }
    }
}