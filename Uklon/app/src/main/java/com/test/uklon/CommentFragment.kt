package com.test.uklon

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.uklon.api.Api
import com.test.uklon.api.models.Comment
import com.test.uklon.api.models.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_comment.*
import kotlinx.android.synthetic.main.fragment_comment.view.*
import kotlinx.android.synthetic.main.item_user.view.*
import java.util.concurrent.TimeUnit

class CommentFragment : BaseFragment() {

    companion object {

        private val USER_ID = "USER_ID"
        private val POST_ID = "POST_ID"

        fun newInstance(userId: Int, postId: Int): CommentFragment {
            val fragment = CommentFragment()
            val args = Bundle()
            args.putInt(USER_ID, userId)
            args.putInt(POST_ID, postId)
            fragment.arguments = args
            return fragment
        }
    }

    private var userId: Int? = null
    private var postId: Int? = null
    private var recyclerView: RecyclerView? = null
    private var swipeLayout: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            userId = arguments.getInt(USER_ID)
            postId = arguments.getInt(POST_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comment, null)
        this.recyclerView = view.recyclerView
        this.recyclerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        this.swipeLayout = view.swipeLayout
        this.swipeLayout?.setOnRefreshListener { loadUser() }
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        loadUser()
    }

    private fun loadComments() {
        val commentsObservable = Api.apiService.comments(postId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { list -> setAdapter(list) },
                        { error -> onError() }
                )

        subscriptions.add(commentsObservable)
    }

    private fun loadUser(){
        val userObservable = Api.apiService.user(userId)
                .subscribeOn(Schedulers.newThread())
                .delay(1000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { user -> fillUserData(user) },
                        { error -> onError()}
                )

        subscriptions.add(userObservable)
    }

    private fun fillUserData(user: User){
        loadComments()
        name.text = user.name
        username.text = user.username
        phone.text = user.phone
        website.text = user.website
        company.text = user.company.toString()
        address.text = user.address.toString()
    }

    private fun onError(){
        showToast()
        this.swipeLayout?.isRefreshing = false
    }

    private fun setAdapter(list: List<Comment>){
        this.recyclerView?.adapter = CommentAdapter(list)
        this.swipeLayout?.isRefreshing = false
    }


}
