package com.test.uklon

import android.support.v4.app.Fragment
import android.widget.Toast
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {
    val subscriptions = CompositeDisposable()
//asdasd
    fun showToast(){
        Toast.makeText(activity, "Error! Swipe", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        subscriptions.clear()
        super.onDestroy()
    }
}