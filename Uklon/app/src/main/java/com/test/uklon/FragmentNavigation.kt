package com.test.uklon

import com.test.uklon.api.models.Post

interface FragmentNavigation {
    fun showCommentFragment(post: Post)
}