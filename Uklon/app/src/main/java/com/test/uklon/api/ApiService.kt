package com.test.uklon.api

import com.test.uklon.api.models.Comment
import com.test.uklon.api.models.Post
import com.test.uklon.api.models.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("/posts")
    fun posts(): Observable<List<Post>>

    @GET("/posts/{id}/comments")
    fun comments(@Path("id") id: Int?): Observable<List<Comment>>

    @GET("/users/{id}")
    fun user(@Path("id") id: Int?): Observable<User>
}