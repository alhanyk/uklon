package com.test.uklon

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.test.uklon.api.models.Comment
import com.test.uklon.util.inflate
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter(val comments: List<Comment>) : RecyclerView.Adapter<CommentAdapter.CommentHolder>() {
//sdfsdf
    override fun getItemCount(): Int = comments.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CommentHolder(parent.inflate(R.layout.item_comment))

    override fun onBindViewHolder(holder: CommentHolder, position: Int){
        holder.bind(comments[position])
    }

    data class CommentHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bind(comment: Comment) = with(view){
            nameComment.text = comment.name
            emailComment.text = comment.email
            bodyComment.text = comment.body
        }
    }
}