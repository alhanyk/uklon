package com.test.uklon

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.test.uklon.api.models.Post

class MainActivity : AppCompatActivity(), FragmentNavigation {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(R.id.container, PostFragment()).commit()
    }

    override fun showCommentFragment(post: Post) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, CommentFragment.newInstance(post.id, post.userId))
                .addToBackStack(null)
                .commit()
    }
}
