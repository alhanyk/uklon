package com.test.uklon

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.uklon.api.Api
import com.test.uklon.api.models.Post
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_post.view.*

class PostFragment : BaseFragment() {

    var recyclerView: RecyclerView? = null
    var swipeLayout: SwipeRefreshLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_post, null)
        this.recyclerView = view.recyclerView
        this.recyclerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        this.swipeLayout = view.swipeLayout
        this.swipeLayout?.setOnRefreshListener { loadData() }
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        loadData()
    }

    fun loadData(){
        val disposable = Api.apiService.posts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {list -> setAdapter(list)},
                        {error -> onError()}
                )

        subscriptions.add(disposable)
    }

    private fun onError(){
        showToast()
        this.swipeLayout?.isRefreshing = false
    }

    private fun setAdapter(list: List<Post>){
        this.recyclerView?.adapter = PostAdapter(list, {showFragment(it)})
        this.swipeLayout?.isRefreshing = false
    }

    private fun showFragment(post: Post){
        if(activity is FragmentNavigation)
            (activity as FragmentNavigation).showCommentFragment(post)
    }
}